// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TUTORIAL_3_1_AISplinePathing_generated_h
#error "AISplinePathing.generated.h already included, missing '#pragma once' in AISplinePathing.h"
#endif
#define TUTORIAL_3_1_AISplinePathing_generated_h

#define Maze_Source_Tutorial_3_1_AISplinePathing_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetNextPoints) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetNextPoints(); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_AISplinePathing_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetNextPoints) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetNextPoints(); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_AISplinePathing_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAISplinePathing(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AAISplinePathing(); \
public: \
	DECLARE_CLASS(AAISplinePathing, AAIController, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AAISplinePathing) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_AISplinePathing_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAAISplinePathing(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AAISplinePathing(); \
public: \
	DECLARE_CLASS(AAISplinePathing, AAIController, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AAISplinePathing) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_AISplinePathing_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAISplinePathing(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAISplinePathing) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAISplinePathing); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAISplinePathing); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAISplinePathing(AAISplinePathing&&); \
	NO_API AAISplinePathing(const AAISplinePathing&); \
public:


#define Maze_Source_Tutorial_3_1_AISplinePathing_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAISplinePathing(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAISplinePathing(AAISplinePathing&&); \
	NO_API AAISplinePathing(const AAISplinePathing&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAISplinePathing); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAISplinePathing); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAISplinePathing)


#define Maze_Source_Tutorial_3_1_AISplinePathing_h_21_PRIVATE_PROPERTY_OFFSET
#define Maze_Source_Tutorial_3_1_AISplinePathing_h_18_PROLOG
#define Maze_Source_Tutorial_3_1_AISplinePathing_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_AISplinePathing_h_21_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_AISplinePathing_h_21_RPC_WRAPPERS \
	Maze_Source_Tutorial_3_1_AISplinePathing_h_21_INCLASS \
	Maze_Source_Tutorial_3_1_AISplinePathing_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Maze_Source_Tutorial_3_1_AISplinePathing_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_AISplinePathing_h_21_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_AISplinePathing_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_AISplinePathing_h_21_INCLASS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_AISplinePathing_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Maze_Source_Tutorial_3_1_AISplinePathing_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
