// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "CameraControl.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraControl() {}
// Cross Module References
	TUTORIAL_3_1_API UClass* Z_Construct_UClass_ACameraControl_NoRegister();
	TUTORIAL_3_1_API UClass* Z_Construct_UClass_ACameraControl();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Tutorial_3_1();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void ACameraControl::StaticRegisterNativesACameraControl()
	{
	}
	UClass* Z_Construct_UClass_ACameraControl_NoRegister()
	{
		return ACameraControl::StaticClass();
	}
	UClass* Z_Construct_UClass_ACameraControl()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Tutorial_3_1,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "CameraControl.h" },
				{ "ModuleRelativePath", "CameraControl.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[] = {
				{ "Category", "CameraControl" },
				{ "ModuleRelativePath", "CameraControl.h" },
				{ "ToolTip", "as this pointer\nCreate input for selected actor" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FollowCamera = { UE4CodeGen_Private::EPropertyClass::Object, "FollowCamera", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ACameraControl, FollowCamera), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(NewProp_FollowCamera_MetaData, ARRAY_COUNT(NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraTwo_MetaData[] = {
				{ "Category", "CameraControl" },
				{ "ModuleRelativePath", "CameraControl.h" },
				{ "ToolTip", "as this pointer\nCreate input for selected actor" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraTwo = { UE4CodeGen_Private::EPropertyClass::Object, "CameraTwo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ACameraControl, CameraTwo), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(NewProp_CameraTwo_MetaData, ARRAY_COUNT(NewProp_CameraTwo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraOne_MetaData[] = {
				{ "Category", "CameraControl" },
				{ "ModuleRelativePath", "CameraControl.h" },
				{ "ToolTip", "Declare player controller pointer for PlayerPawn access\nCreate input for selected actor" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraOne = { UE4CodeGen_Private::EPropertyClass::Object, "CameraOne", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ACameraControl, CameraOne), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(NewProp_CameraOne_MetaData, ARRAY_COUNT(NewProp_CameraOne_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FollowCamera,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CameraTwo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CameraOne,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ACameraControl>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ACameraControl::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACameraControl, 1933988063);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACameraControl(Z_Construct_UClass_ACameraControl, &ACameraControl::StaticClass, TEXT("/Script/Tutorial_3_1"), TEXT("ACameraControl"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACameraControl);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
