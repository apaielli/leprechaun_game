// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "AIControl.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAIControl() {}
// Cross Module References
	TUTORIAL_3_1_API UClass* Z_Construct_UClass_AAIControl_NoRegister();
	TUTORIAL_3_1_API UClass* Z_Construct_UClass_AAIControl();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_Tutorial_3_1();
	TUTORIAL_3_1_API UFunction* Z_Construct_UFunction_AAIControl_GetRandomPOI();
	ENGINE_API UClass* Z_Construct_UClass_ATargetPoint_NoRegister();
	TUTORIAL_3_1_API UFunction* Z_Construct_UFunction_AAIControl_GoNextPOI();
	TUTORIAL_3_1_API UFunction* Z_Construct_UFunction_AAIControl_GoToRandomPOI();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void AAIControl::StaticRegisterNativesAAIControl()
	{
		UClass* Class = AAIControl::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetRandomPOI", &AAIControl::execGetRandomPOI },
			{ "GoNextPOI", &AAIControl::execGoNextPOI },
			{ "GoToRandomPOI", &AAIControl::execGoToRandomPOI },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_AAIControl_GetRandomPOI()
	{
		struct AIControl_eventGetRandomPOI_Parms
		{
			ATargetPoint* ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(AIControl_eventGetRandomPOI_Parms, ReturnValue), Z_Construct_UClass_ATargetPoint_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "AIControl.h" },
				{ "ToolTip", "Pointer for retrieving random target point" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AAIControl, "GetRandomPOI", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00040401, sizeof(AIControl_eventGetRandomPOI_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AAIControl_GoNextPOI()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "AIControl.h" },
				{ "ToolTip", "Function for moving the AI character to the next target point in the order" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AAIControl, "GoNextPOI", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AAIControl_GoToRandomPOI()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "AIControl.h" },
				{ "ToolTip", "Function for moving the AI character to a random target point" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AAIControl, "GoToRandomPOI", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AAIControl_NoRegister()
	{
		return AAIControl::StaticClass();
	}
	UClass* Z_Construct_UClass_AAIControl()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AAIController,
				(UObject* (*)())Z_Construct_UPackage__Script_Tutorial_3_1,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_AAIControl_GetRandomPOI, "GetRandomPOI" }, // 1329296194
				{ &Z_Construct_UFunction_AAIControl_GoNextPOI, "GoNextPOI" }, // 445833389
				{ &Z_Construct_UFunction_AAIControl_GoToRandomPOI, "GoToRandomPOI" }, // 3841972642
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "AIControl.h" },
				{ "ModuleRelativePath", "AIControl.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoIs_MetaData[] = {
				{ "ModuleRelativePath", "AIControl.h" },
				{ "ToolTip", "Array for storing all actor location of target point type" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PoIs = { UE4CodeGen_Private::EPropertyClass::Array, "PoIs", RF_Public|RF_Transient|RF_MarkAsNative, 0x0040000000000000, 1, nullptr, STRUCT_OFFSET(AAIControl, PoIs), METADATA_PARAMS(NewProp_PoIs_MetaData, ARRAY_COUNT(NewProp_PoIs_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PoIs_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "PoIs", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsPathRandom_MetaData[] = {
				{ "Category", "AIControl" },
				{ "ModuleRelativePath", "AIControl.h" },
				{ "ToolTip", "Boolean variable for enabling and disabling random pathing" },
			};
#endif
			auto NewProp_bIsPathRandom_SetBit = [](void* Obj){ ((AAIControl*)Obj)->bIsPathRandom = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsPathRandom = { UE4CodeGen_Private::EPropertyClass::Bool, "bIsPathRandom", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AAIControl), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bIsPathRandom_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bIsPathRandom_MetaData, ARRAY_COUNT(NewProp_bIsPathRandom_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_PoIs,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_PoIs_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bIsPathRandom,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AAIControl>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AAIControl::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900280u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAIControl, 1565632393);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAIControl(Z_Construct_UClass_AAIControl, &AAIControl::StaticClass, TEXT("/Script/Tutorial_3_1"), TEXT("AAIControl"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAIControl);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
