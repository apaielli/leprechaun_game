// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "SphereMovementComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSphereMovementComponent() {}
// Cross Module References
	TUTORIAL_3_1_API UClass* Z_Construct_UClass_USphereMovementComponent_NoRegister();
	TUTORIAL_3_1_API UClass* Z_Construct_UClass_USphereMovementComponent();
	ENGINE_API UClass* Z_Construct_UClass_UPawnMovementComponent();
	UPackage* Z_Construct_UPackage__Script_Tutorial_3_1();
// End Cross Module References
	void USphereMovementComponent::StaticRegisterNativesUSphereMovementComponent()
	{
	}
	UClass* Z_Construct_UClass_USphereMovementComponent_NoRegister()
	{
		return USphereMovementComponent::StaticClass();
	}
	UClass* Z_Construct_UClass_USphereMovementComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_UPawnMovementComponent,
				(UObject* (*)())Z_Construct_UPackage__Script_Tutorial_3_1,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "SphereMovementComponent.h" },
				{ "ModuleRelativePath", "SphereMovementComponent.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<USphereMovementComponent>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&USphereMovementComponent::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00B00084u,
				nullptr, 0,
				nullptr, 0,
				"Engine",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USphereMovementComponent, 2044459722);
	static FCompiledInDefer Z_CompiledInDefer_UClass_USphereMovementComponent(Z_Construct_UClass_USphereMovementComponent, &USphereMovementComponent::StaticClass, TEXT("/Script/Tutorial_3_1"), TEXT("USphereMovementComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USphereMovementComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
