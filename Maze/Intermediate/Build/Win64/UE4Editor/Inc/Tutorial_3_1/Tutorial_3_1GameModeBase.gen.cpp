// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Tutorial_3_1GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTutorial_3_1GameModeBase() {}
// Cross Module References
	TUTORIAL_3_1_API UClass* Z_Construct_UClass_ATutorial_3_1GameModeBase_NoRegister();
	TUTORIAL_3_1_API UClass* Z_Construct_UClass_ATutorial_3_1GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Tutorial_3_1();
// End Cross Module References
	void ATutorial_3_1GameModeBase::StaticRegisterNativesATutorial_3_1GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ATutorial_3_1GameModeBase_NoRegister()
	{
		return ATutorial_3_1GameModeBase::StaticClass();
	}
	UClass* Z_Construct_UClass_ATutorial_3_1GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_Tutorial_3_1,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "Tutorial_3_1GameModeBase.h" },
				{ "ModuleRelativePath", "Tutorial_3_1GameModeBase.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ATutorial_3_1GameModeBase>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ATutorial_3_1GameModeBase::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATutorial_3_1GameModeBase, 3407034978);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATutorial_3_1GameModeBase(Z_Construct_UClass_ATutorial_3_1GameModeBase, &ATutorial_3_1GameModeBase::StaticClass, TEXT("/Script/Tutorial_3_1"), TEXT("ATutorial_3_1GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATutorial_3_1GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
