// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TUTORIAL_3_1_GAMPlayerController_generated_h
#error "GAMPlayerController.generated.h already included, missing '#pragma once' in GAMPlayerController.h"
#endif
#define TUTORIAL_3_1_GAMPlayerController_generated_h

#define Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_RPC_WRAPPERS
#define Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAMPlayerController(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AGAMPlayerController(); \
public: \
	DECLARE_CLASS(AGAMPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AGAMPlayerController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAGAMPlayerController(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AGAMPlayerController(); \
public: \
	DECLARE_CLASS(AGAMPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AGAMPlayerController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAMPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAMPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAMPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAMPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAMPlayerController(AGAMPlayerController&&); \
	NO_API AGAMPlayerController(const AGAMPlayerController&); \
public:


#define Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAMPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAMPlayerController(AGAMPlayerController&&); \
	NO_API AGAMPlayerController(const AGAMPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAMPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAMPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAMPlayerController)


#define Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_PRIVATE_PROPERTY_OFFSET
#define Maze_Source_Tutorial_3_1_GAMPlayerController_h_12_PROLOG
#define Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_RPC_WRAPPERS \
	Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_INCLASS \
	Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_INCLASS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_GAMPlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Maze_Source_Tutorial_3_1_GAMPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
