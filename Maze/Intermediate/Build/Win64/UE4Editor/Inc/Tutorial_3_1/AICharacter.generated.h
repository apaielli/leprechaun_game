// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class APawn;
#ifdef TUTORIAL_3_1_AICharacter_generated_h
#error "AICharacter.generated.h already included, missing '#pragma once' in AICharacter.h"
#endif
#define TUTORIAL_3_1_AICharacter_generated_h

#define Maze_Source_Tutorial_3_1_AICharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPlayerSeen) \
	{ \
		P_GET_OBJECT(APawn,Z_Param_Pawn); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPlayerSeen(Z_Param_Pawn); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_AICharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPlayerSeen) \
	{ \
		P_GET_OBJECT(APawn,Z_Param_Pawn); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPlayerSeen(Z_Param_Pawn); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_AICharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAICharacter(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AAICharacter(); \
public: \
	DECLARE_CLASS(AAICharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AAICharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_AICharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAAICharacter(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AAICharacter(); \
public: \
	DECLARE_CLASS(AAICharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AAICharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_AICharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAICharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAICharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAICharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAICharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAICharacter(AAICharacter&&); \
	NO_API AAICharacter(const AAICharacter&); \
public:


#define Maze_Source_Tutorial_3_1_AICharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAICharacter(AAICharacter&&); \
	NO_API AAICharacter(const AAICharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAICharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAICharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAICharacter)


#define Maze_Source_Tutorial_3_1_AICharacter_h_14_PRIVATE_PROPERTY_OFFSET
#define Maze_Source_Tutorial_3_1_AICharacter_h_11_PROLOG
#define Maze_Source_Tutorial_3_1_AICharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_AICharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_AICharacter_h_14_RPC_WRAPPERS \
	Maze_Source_Tutorial_3_1_AICharacter_h_14_INCLASS \
	Maze_Source_Tutorial_3_1_AICharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Maze_Source_Tutorial_3_1_AICharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_AICharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_AICharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_AICharacter_h_14_INCLASS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_AICharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Maze_Source_Tutorial_3_1_AICharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
