// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TUTORIAL_3_1_PlayerPawn_generated_h
#error "PlayerPawn.generated.h already included, missing '#pragma once' in PlayerPawn.h"
#endif
#define TUTORIAL_3_1_PlayerPawn_generated_h

#define Maze_Source_Tutorial_3_1_PlayerPawn_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnStopJump) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnStopJump(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnStartJump) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnStartJump(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCountdown) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetCountdown(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetGoldCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetGoldCount(); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_PlayerPawn_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnStopJump) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnStopJump(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnStartJump) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnStartJump(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCountdown) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetCountdown(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetGoldCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetGoldCount(); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_PlayerPawn_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPawn(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_APlayerPawn(); \
public: \
	DECLARE_CLASS(APlayerPawn, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawn) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_PlayerPawn_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPawn(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_APlayerPawn(); \
public: \
	DECLARE_CLASS(APlayerPawn, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawn) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_PlayerPawn_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawn(APlayerPawn&&); \
	NO_API APlayerPawn(const APlayerPawn&); \
public:


#define Maze_Source_Tutorial_3_1_PlayerPawn_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawn(APlayerPawn&&); \
	NO_API APlayerPawn(const APlayerPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPawn)


#define Maze_Source_Tutorial_3_1_PlayerPawn_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(APlayerPawn, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(APlayerPawn, FollowCamera); }


#define Maze_Source_Tutorial_3_1_PlayerPawn_h_11_PROLOG
#define Maze_Source_Tutorial_3_1_PlayerPawn_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_PlayerPawn_h_14_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_PlayerPawn_h_14_RPC_WRAPPERS \
	Maze_Source_Tutorial_3_1_PlayerPawn_h_14_INCLASS \
	Maze_Source_Tutorial_3_1_PlayerPawn_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Maze_Source_Tutorial_3_1_PlayerPawn_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_PlayerPawn_h_14_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_PlayerPawn_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_PlayerPawn_h_14_INCLASS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_PlayerPawn_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Maze_Source_Tutorial_3_1_PlayerPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
