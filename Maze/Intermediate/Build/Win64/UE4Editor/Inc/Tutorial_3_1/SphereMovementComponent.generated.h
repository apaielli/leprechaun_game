// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TUTORIAL_3_1_SphereMovementComponent_generated_h
#error "SphereMovementComponent.generated.h already included, missing '#pragma once' in SphereMovementComponent.h"
#endif
#define TUTORIAL_3_1_SphereMovementComponent_generated_h

#define Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_RPC_WRAPPERS
#define Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSphereMovementComponent(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_USphereMovementComponent(); \
public: \
	DECLARE_CLASS(USphereMovementComponent, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(USphereMovementComponent) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSphereMovementComponent(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_USphereMovementComponent(); \
public: \
	DECLARE_CLASS(USphereMovementComponent, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(USphereMovementComponent) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USphereMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USphereMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USphereMovementComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USphereMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USphereMovementComponent(USphereMovementComponent&&); \
	NO_API USphereMovementComponent(const USphereMovementComponent&); \
public:


#define Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USphereMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USphereMovementComponent(USphereMovementComponent&&); \
	NO_API USphereMovementComponent(const USphereMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USphereMovementComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USphereMovementComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USphereMovementComponent)


#define Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define Maze_Source_Tutorial_3_1_SphereMovementComponent_h_12_PROLOG
#define Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_RPC_WRAPPERS \
	Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_INCLASS \
	Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_SphereMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Maze_Source_Tutorial_3_1_SphereMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
