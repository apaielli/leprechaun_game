// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATargetPoint;
#ifdef TUTORIAL_3_1_AIControl_generated_h
#error "AIControl.generated.h already included, missing '#pragma once' in AIControl.h"
#endif
#define TUTORIAL_3_1_AIControl_generated_h

#define Maze_Source_Tutorial_3_1_AIControl_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGoNextPOI) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GoNextPOI(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGoToRandomPOI) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GoToRandomPOI(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRandomPOI) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ATargetPoint**)Z_Param__Result=P_THIS->GetRandomPOI(); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_AIControl_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGoNextPOI) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GoNextPOI(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGoToRandomPOI) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GoToRandomPOI(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRandomPOI) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ATargetPoint**)Z_Param__Result=P_THIS->GetRandomPOI(); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_AIControl_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAIControl(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AAIControl(); \
public: \
	DECLARE_CLASS(AAIControl, AAIController, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AAIControl) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_AIControl_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAAIControl(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AAIControl(); \
public: \
	DECLARE_CLASS(AAIControl, AAIController, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AAIControl) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_AIControl_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAIControl(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAIControl) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAIControl); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAIControl); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAIControl(AAIControl&&); \
	NO_API AAIControl(const AAIControl&); \
public:


#define Maze_Source_Tutorial_3_1_AIControl_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAIControl(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAIControl(AAIControl&&); \
	NO_API AAIControl(const AAIControl&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAIControl); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAIControl); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAIControl)


#define Maze_Source_Tutorial_3_1_AIControl_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PoIs() { return STRUCT_OFFSET(AAIControl, PoIs); }


#define Maze_Source_Tutorial_3_1_AIControl_h_16_PROLOG
#define Maze_Source_Tutorial_3_1_AIControl_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_AIControl_h_19_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_AIControl_h_19_RPC_WRAPPERS \
	Maze_Source_Tutorial_3_1_AIControl_h_19_INCLASS \
	Maze_Source_Tutorial_3_1_AIControl_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Maze_Source_Tutorial_3_1_AIControl_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_AIControl_h_19_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_AIControl_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_AIControl_h_19_INCLASS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_AIControl_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Maze_Source_Tutorial_3_1_AIControl_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
