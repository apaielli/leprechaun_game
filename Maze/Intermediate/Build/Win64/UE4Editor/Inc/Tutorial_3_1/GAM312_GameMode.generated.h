// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UUserWidget;
#ifdef TUTORIAL_3_1_GAM312_GameMode_generated_h
#error "GAM312_GameMode.generated.h already included, missing '#pragma once' in GAM312_GameMode.h"
#endif
#define TUTORIAL_3_1_GAM312_GameMode_generated_h

#define Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execChangeMenuWidget) \
	{ \
		P_GET_OBJECT(UClass,Z_Param_NewWidgetClass); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ChangeMenuWidget(Z_Param_NewWidgetClass); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execChangeMenuWidget) \
	{ \
		P_GET_OBJECT(UClass,Z_Param_NewWidgetClass); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ChangeMenuWidget(Z_Param_NewWidgetClass); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAM312_GameMode(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AGAM312_GameMode(); \
public: \
	DECLARE_CLASS(AGAM312_GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AGAM312_GameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAGAM312_GameMode(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AGAM312_GameMode(); \
public: \
	DECLARE_CLASS(AGAM312_GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AGAM312_GameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAM312_GameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAM312_GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAM312_GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM312_GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAM312_GameMode(AGAM312_GameMode&&); \
	NO_API AGAM312_GameMode(const AGAM312_GameMode&); \
public:


#define Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAM312_GameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAM312_GameMode(AGAM312_GameMode&&); \
	NO_API AGAM312_GameMode(const AGAM312_GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAM312_GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM312_GameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAM312_GameMode)


#define Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StartingWidget() { return STRUCT_OFFSET(AGAM312_GameMode, StartingWidget); } \
	FORCEINLINE static uint32 __PPO__CurrentWidget() { return STRUCT_OFFSET(AGAM312_GameMode, CurrentWidget); }


#define Maze_Source_Tutorial_3_1_GAM312_GameMode_h_13_PROLOG
#define Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_RPC_WRAPPERS \
	Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_INCLASS \
	Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_INCLASS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_GAM312_GameMode_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Maze_Source_Tutorial_3_1_GAM312_GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
