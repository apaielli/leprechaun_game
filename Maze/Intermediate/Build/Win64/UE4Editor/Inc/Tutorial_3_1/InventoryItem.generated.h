// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TUTORIAL_3_1_InventoryItem_generated_h
#error "InventoryItem.generated.h already included, missing '#pragma once' in InventoryItem.h"
#endif
#define TUTORIAL_3_1_InventoryItem_generated_h

#define Maze_Source_Tutorial_3_1_InventoryItem_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInteract) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Interact(); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_InventoryItem_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInteract) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Interact(); \
		P_NATIVE_END; \
	}


#define Maze_Source_Tutorial_3_1_InventoryItem_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAInventoryItem(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AInventoryItem(); \
public: \
	DECLARE_CLASS(AInventoryItem, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AInventoryItem) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_InventoryItem_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAInventoryItem(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_AInventoryItem(); \
public: \
	DECLARE_CLASS(AInventoryItem, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(AInventoryItem) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_InventoryItem_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AInventoryItem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AInventoryItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AInventoryItem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AInventoryItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AInventoryItem(AInventoryItem&&); \
	NO_API AInventoryItem(const AInventoryItem&); \
public:


#define Maze_Source_Tutorial_3_1_InventoryItem_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AInventoryItem(AInventoryItem&&); \
	NO_API AInventoryItem(const AInventoryItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AInventoryItem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AInventoryItem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AInventoryItem)


#define Maze_Source_Tutorial_3_1_InventoryItem_h_12_PRIVATE_PROPERTY_OFFSET
#define Maze_Source_Tutorial_3_1_InventoryItem_h_9_PROLOG
#define Maze_Source_Tutorial_3_1_InventoryItem_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_InventoryItem_h_12_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_InventoryItem_h_12_RPC_WRAPPERS \
	Maze_Source_Tutorial_3_1_InventoryItem_h_12_INCLASS \
	Maze_Source_Tutorial_3_1_InventoryItem_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Maze_Source_Tutorial_3_1_InventoryItem_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_InventoryItem_h_12_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_InventoryItem_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_InventoryItem_h_12_INCLASS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_InventoryItem_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Maze_Source_Tutorial_3_1_InventoryItem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
