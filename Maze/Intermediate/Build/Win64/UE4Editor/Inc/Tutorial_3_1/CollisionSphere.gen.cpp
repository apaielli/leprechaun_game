// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "CollisionSphere.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCollisionSphere() {}
// Cross Module References
	TUTORIAL_3_1_API UClass* Z_Construct_UClass_ACollisionSphere_NoRegister();
	TUTORIAL_3_1_API UClass* Z_Construct_UClass_ACollisionSphere();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_Tutorial_3_1();
// End Cross Module References
	void ACollisionSphere::StaticRegisterNativesACollisionSphere()
	{
	}
	UClass* Z_Construct_UClass_ACollisionSphere_NoRegister()
	{
		return ACollisionSphere::StaticClass();
	}
	UClass* Z_Construct_UClass_ACollisionSphere()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_APawn,
				(UObject* (*)())Z_Construct_UPackage__Script_Tutorial_3_1,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Navigation" },
				{ "IncludePath", "CollisionSphere.h" },
				{ "ModuleRelativePath", "CollisionSphere.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ACollisionSphere>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ACollisionSphere::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACollisionSphere, 3652686888);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACollisionSphere(Z_Construct_UClass_ACollisionSphere, &ACollisionSphere::StaticClass, TEXT("/Script/Tutorial_3_1"), TEXT("ACollisionSphere"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACollisionSphere);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
