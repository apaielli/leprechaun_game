// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TUTORIAL_3_1_CameraControl_generated_h
#error "CameraControl.generated.h already included, missing '#pragma once' in CameraControl.h"
#endif
#define TUTORIAL_3_1_CameraControl_generated_h

#define Maze_Source_Tutorial_3_1_CameraControl_h_12_RPC_WRAPPERS
#define Maze_Source_Tutorial_3_1_CameraControl_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Maze_Source_Tutorial_3_1_CameraControl_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACameraControl(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_ACameraControl(); \
public: \
	DECLARE_CLASS(ACameraControl, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(ACameraControl) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_CameraControl_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACameraControl(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_ACameraControl(); \
public: \
	DECLARE_CLASS(ACameraControl, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(ACameraControl) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_CameraControl_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACameraControl(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACameraControl) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraControl); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraControl); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraControl(ACameraControl&&); \
	NO_API ACameraControl(const ACameraControl&); \
public:


#define Maze_Source_Tutorial_3_1_CameraControl_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraControl(ACameraControl&&); \
	NO_API ACameraControl(const ACameraControl&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraControl); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraControl); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACameraControl)


#define Maze_Source_Tutorial_3_1_CameraControl_h_12_PRIVATE_PROPERTY_OFFSET
#define Maze_Source_Tutorial_3_1_CameraControl_h_9_PROLOG
#define Maze_Source_Tutorial_3_1_CameraControl_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_CameraControl_h_12_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_CameraControl_h_12_RPC_WRAPPERS \
	Maze_Source_Tutorial_3_1_CameraControl_h_12_INCLASS \
	Maze_Source_Tutorial_3_1_CameraControl_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Maze_Source_Tutorial_3_1_CameraControl_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_CameraControl_h_12_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_CameraControl_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_CameraControl_h_12_INCLASS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_CameraControl_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Maze_Source_Tutorial_3_1_CameraControl_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
