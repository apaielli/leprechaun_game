// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TUTORIAL_3_1_CollisionSphere_generated_h
#error "CollisionSphere.generated.h already included, missing '#pragma once' in CollisionSphere.h"
#endif
#define TUTORIAL_3_1_CollisionSphere_generated_h

#define Maze_Source_Tutorial_3_1_CollisionSphere_h_12_RPC_WRAPPERS
#define Maze_Source_Tutorial_3_1_CollisionSphere_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Maze_Source_Tutorial_3_1_CollisionSphere_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACollisionSphere(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_ACollisionSphere(); \
public: \
	DECLARE_CLASS(ACollisionSphere, APawn, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(ACollisionSphere) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_CollisionSphere_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACollisionSphere(); \
	friend TUTORIAL_3_1_API class UClass* Z_Construct_UClass_ACollisionSphere(); \
public: \
	DECLARE_CLASS(ACollisionSphere, APawn, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Tutorial_3_1"), NO_API) \
	DECLARE_SERIALIZER(ACollisionSphere) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Maze_Source_Tutorial_3_1_CollisionSphere_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACollisionSphere(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACollisionSphere) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACollisionSphere); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACollisionSphere); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACollisionSphere(ACollisionSphere&&); \
	NO_API ACollisionSphere(const ACollisionSphere&); \
public:


#define Maze_Source_Tutorial_3_1_CollisionSphere_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACollisionSphere(ACollisionSphere&&); \
	NO_API ACollisionSphere(const ACollisionSphere&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACollisionSphere); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACollisionSphere); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACollisionSphere)


#define Maze_Source_Tutorial_3_1_CollisionSphere_h_12_PRIVATE_PROPERTY_OFFSET
#define Maze_Source_Tutorial_3_1_CollisionSphere_h_9_PROLOG
#define Maze_Source_Tutorial_3_1_CollisionSphere_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_CollisionSphere_h_12_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_CollisionSphere_h_12_RPC_WRAPPERS \
	Maze_Source_Tutorial_3_1_CollisionSphere_h_12_INCLASS \
	Maze_Source_Tutorial_3_1_CollisionSphere_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Maze_Source_Tutorial_3_1_CollisionSphere_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Maze_Source_Tutorial_3_1_CollisionSphere_h_12_PRIVATE_PROPERTY_OFFSET \
	Maze_Source_Tutorial_3_1_CollisionSphere_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_CollisionSphere_h_12_INCLASS_NO_PURE_DECLS \
	Maze_Source_Tutorial_3_1_CollisionSphere_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Maze_Source_Tutorial_3_1_CollisionSphere_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
