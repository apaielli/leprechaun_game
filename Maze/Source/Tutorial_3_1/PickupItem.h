// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupItem.generated.h"


UCLASS()
class TUTORIAL_3_1_API APickupItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickupItem();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	USceneComponent* PickupItemRoot;		// Declaration of root pointer

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PickupItemMesh;	// Declaration of mesh pointer with property access in to engine (allows you to mick mesh)

	UPROPERTY(EditAnywhere)
	UShapeComponent* PickupItemBox;			// Declaration of overlap box pointer

	UFUNCTION()
	void OnPlayerEnterBox(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);	// Overlap function

};
