// Fill out your copyright notice in the Description page of Project Settings.

#include "CollisionSphere.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "ConstructorHelpers.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "SphereMovementComponent.h"



// Sets default values
ACollisionSphere::ACollisionSphere()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	USphereComponent* SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent")); // Initialization of Sphere componenets pointer
	RootComponent = SphereComp;																		// Assignmenet of root component
	SphereComp->InitSphereRadius(40.0f);															// Set sphere components size
	SphereComp->SetCollisionProfileName(TEXT("Pawn"));												// Set sphere components collision perameters

	UStaticMeshComponent* SphereObject = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));										// Initialization of Static mesh componenet
	SphereObject->SetupAttachment(RootComponent);																							// Attachment of mesh to root components
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));	// Assignment of static mesh asset as reference
	if (SphereVisualAsset.Succeeded())															// If the static mesh asset loads
	{
		SphereObject->SetStaticMesh(SphereVisualAsset.Object);									// Set static mesh object
		SphereObject->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));							// Set static mesh location 
		SphereObject->SetWorldScale3D(FVector(0.8f));											// Set static mesh scale
	}
	USpringArmComponent* SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("AttachmentArm"));	// Initialization of spring arm componenets
	SpringArm->SetupAttachment(RootComponent);																// Attachment of spring arm to root component
	SpringArm->RelativeRotation = FRotator(-45.0f, 0.0f, 0.0f);												// Rotate spring arm location to back of root
	SpringArm->TargetArmLength = 400.0f;																	// Set spring arm length (camera distance)
	SpringArm->bEnableCameraLag = true;																		// Set a lag effect to enabled
	SpringArm->CameraLagSpeed = 3.0f;																		// Rate at which spring arm lag behind root movement

	UCameraComponent* ObjCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("SphereCamera"));			// Initialization of camera component
	ObjCamera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);									// Attachment of camera component to spring arm



	ObjMovementComponent = CreateDefaultSubobject<USphereMovementComponent>(TEXT("OSMovementComponent"));	// Initialization od movement component
	ObjMovementComponent->UpdatedComponent = RootComponent;													// Add movement component to the Root
}

// Called when the game starts or when spawned
void ACollisionSphere::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACollisionSphere::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACollisionSphere::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAxis("Forwards", this, &ACollisionSphere::ForwardBack);		// Binding of forward and backward movement keys
	InputComponent->BindAxis("Strafe", this, &ACollisionSphere::Strafe);			// Binding of side to side movement keys
	InputComponent->BindAxis("Turn", this, &ACollisionSphere::Turn);				// Binding of mouse turning
}
UPawnMovementComponent* ACollisionSphere::GetMovementComponent() const
{
	return ObjMovementComponent;				// Function for returning the Sphere movement component perameters
}

void ACollisionSphere::ForwardBack(float value)			//Called on forward and backward movements key usage
{
	if (ObjMovementComponent && (ObjMovementComponent->UpdatedComponent == RootComponent))
	{
		ObjMovementComponent->AddInputVector(GetActorForwardVector() * value);		// Updates actors movements base on forward vector
	}
}

void ACollisionSphere::Strafe(float value)				// Called on strafe movement key usage
{
	if (ObjMovementComponent && (ObjMovementComponent->UpdatedComponent == RootComponent))
	{
		ObjMovementComponent->AddInputVector(GetActorRightVector() * value);		// Updates movements base on the right vector
	}
}

void ACollisionSphere::Turn(float value)				// Called when mouse input for turning is used
{
	FRotator SphRotation = GetActorRotation();			// Get actor current rotation
	SphRotation.Yaw += value;							// Take mouse yaw input plus actors current yaw
	SetActorRotation(SphRotation);						// Updates the actor rotation

}

