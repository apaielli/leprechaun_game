// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class Tutorial_3_1 : ModuleRules
{
	public Tutorial_3_1(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "UMG" }); // C# dependencies (added UMG for UI)

		PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" }); // Private C# dependencies added for UI usage

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
