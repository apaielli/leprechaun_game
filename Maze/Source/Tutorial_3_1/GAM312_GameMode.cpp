// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312_GameMode.h"

// Called when closing the main menu and starting the game level and adding the new UI
void AGAM312_GameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();	// Removes the current UI widget
		CurrentWidget = nullptr;				// Set the current UI  widget to a null pointer
	}

	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);		// Assign the Current  UI widget to be the new widget

		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();											// Add the new UI widget to the screen as the current UI widget
		}
	}

}

void AGAM312_GameMode::BeginPlay()
{
	Super::BeginPlay();
	ChangeMenuWidget(StartingWidget);												// Sets the new UI widget the be the starting UI that is set in the game mode BP API
}
