// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "GAM312_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class TUTORIAL_3_1_API AGAM312_GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = "Game UI")
		void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);	//Function for assigning UI widget

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Game UI")
		TSubclassOf<UUserWidget> StartingWidget;	// API accessible variable for selection UI class widget

	UPROPERTY()
		UUserWidget* CurrentWidget;					// Pointer for UI widget
};
