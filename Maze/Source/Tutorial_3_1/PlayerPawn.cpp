// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerPawn.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SprintSpeedModifier = 4.0f;		// Declared sprint speed modifier



	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = InitRotationRate;
	
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));	// Assgin a spring arm component to the character sub objects
	CameraBoom->SetupAttachment(RootComponent);										// Attaches spring arm to actor root component
	CameraBoom->TargetArmLength = InitCameraZoom;									// Sets spring arm length to default lenght variable
	CameraBoom->bUsePawnControlRotation = true;										// Set the spring arm to follow character rotation
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));	// Assgin a camera component to the character sub objects
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);		// Attaches camera component to the spring arm components socket
	FollowCamera->RelativeLocation = FVector(0, 0, 50 + BaseEyeHeight);				// Aligns camera to first person view
	FollowCamera->bUsePawnControlRotation = false;									// Make camera unable to rotate indipendantly from parent compenent
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	UpdateCameraArmLengths();
	CameraZoom = CameraBoom->TargetArmLength;
	GetWorld()->GetTimerManager().SetTimer(GameTimer, this, &APlayerPawn::GameClock, 1.0f, true);
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Setup of input bindings
void APlayerPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAxis("ForwardBack", this, &APlayerPawn::ForwardBack);	// Forward and Backward movement bindings
	InputComponent->BindAxis("LeftRight", this, &APlayerPawn::LeftRight);		// Side to side strafing movement bindings
	InputComponent->BindAxis("MouseYaw", this, &APlayerPawn::AddControllerYawInput);		//Mouse control of side to side looking
	InputComponent->BindAxis("MousePitch", this, &APlayerPawn::AddControllerPitchInput);	//Mouse control of up and down looking
	InputComponent->BindAxis("TurnAtRate", this, &APlayerPawn::TurnAtRate);					
	InputComponent->BindAxis("LookUpAtRate", this, &APlayerPawn::LookUpAtRate);
	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &APlayerPawn::CameraZoomIn);		//Sets input for camera zoom in
	InputComponent->BindAction("ZoomOut", IE_Pressed, this, &APlayerPawn::CameraZoomOut);	//Sets input for camera zoom out

	InputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerPawn::SprintDelay);		//Sprinting movement on press
	InputComponent->BindAction("Sprint", IE_Released, this, &APlayerPawn::StopSprinting);	//Sprinting end on release

	InputComponent->BindAction("Jump", IE_Pressed, this, &APlayerPawn::OnStartJump);		//Jumping action on press
	InputComponent->BindAction("Jump", IE_Released, this, &APlayerPawn::OnStopJump);		//Jumping action end on release

	InputComponent->BindAction("CameraOne", IE_Pressed, this, &APlayerPawn::ChangeView<1>);		//Sets input for switching camera view to external camera 1
	InputComponent->BindAction("CameraTwo", IE_Pressed, this, &APlayerPawn::ChangeView<2>);		//Sets input for switching camera view to external camera 2
	InputComponent->BindAction("FollowCamera", IE_Pressed, this, &APlayerPawn::ChangeView<0>);	//Sets input for switching to players attached camera component

}

// Called for Forward and Backward movement
void APlayerPawn::ForwardBack(float Value)
{
	if (Controller && Value) {
		FVector Forward = GetActorForwardVector();	// Assign player forward vector to new variable
		AddMovementInput(Forward, Value); 			// When forward or backward binding are pressed, move character in farword or backward from point based on input value
	}
}

// Called for Left and Right Strafing movement
void APlayerPawn::LeftRight(float Value)
{
	if (Controller && Value) {
		FVector Right = GetActorRightVector();	// Assign player right angle vector to new variable
		AddMovementInput(Right, Value);			// When left or right strafe bindings are pressed, move character in direction that is the right angled from the forward vector base on input
	}
}

// Called when play moves mouse left or right
void APlayerPawn::TurnAtRate(float TurnRate)
{
	AddControllerYawInput(TurnRate * BaseTurnRate * GetWorld()->GetDeltaSeconds());		// Allows player to turn  by scaling turn rate
}

// Called when player moves mouse up or down
void APlayerPawn::LookUpAtRate(float UpRate)
{
	AddControllerPitchInput(UpRate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());	// Allows player to look up scaling look up and down rate
}

// Called you update camera distance
void APlayerPawn::UpdateCameraArmLengths()
{
	DefaultArmLength = CameraBoom->TargetArmLength;		// Updates the camera arm length when zooming in or out
}

// Called when zooming in
void APlayerPawn::CameraZoomIn()
{
	if (CameraZoom != MaxCameraZoom)						// If the camera distance is not at its max distance
	{
		CameraZoom -= MaxCameraZoom;						// update the camera to zoom in

		if (CameraZoom <= MaxCameraZoom)					// If camera distance is less than or equal to max distance
		{
			CameraBoom->TargetArmLength = MaxCameraZoom;	// Change camera arm length the max distance
			CameraZoom = MaxCameraZoom;						// change camera distance variable to the max distance
		}
		else
			CameraBoom->TargetArmLength = CameraZoom;		// If none of the over, update camera distance next level of zoom
	}
}

// Called when zooming out
void APlayerPawn::CameraZoomOut()
{
	if (CameraZoom != MinCameraZoom)						// If the camera distance is not at its minmum distance
	{
		CameraZoom += MinCameraZoom;						// update the camera to zoom out

		if (CameraZoom >= MinCameraZoom)					// If camera distance is greater than or equal to minimum distance
		{
			CameraBoom->TargetArmLength = MinCameraZoom;	// Change camera arm length the minmum distance
			CameraZoom = MinCameraZoom;						// change camera distance variable to the minimum distance
		}
		else
			CameraBoom->TargetArmLength = CameraZoom;		// If none of the over, update camera distance next level of zoom
	}
}

// Sprint delay timer 
void APlayerPawn::SprintDelay() {
	GetWorld()->GetTimerManager().SetTimer(SprintDelayHandle, this, &APlayerPawn::Sprint, 1.0f, false); //Delay timer for sprint call of 1 second
}
// Called when player switches camera view
void APlayerPawn::SetView(int camNum)
{
	for (TActorIterator<ACameraControl> ActorItr(GetWorld()); ActorItr; ++ActorItr) // For each actor included in the class control iteration
	{
		if (camNum == 0)						// If input is 0
		{
			ActorItr->SetFollowCamera();		// Set the viewpoint to the assigned actor location (player camera)
			break;
		}
		if (camNum == 1)						// If input is 1
		{
			ActorItr->SetCameraOne();			// Set the viewpoint to the assigned actor location (camera one as set in camera control class)
			break;
		}
		else if (camNum == 2)					// If input is 2
		{
			ActorItr->SetCameraTwo();			// Set the viewpoint to the assigne actor location (camera two as set in camera control class)
			break;
		}
	}
}

// Called when updating the UI to display current gold amount
int APlayerPawn::GetGoldCount()
{
	return goldAmount;
}

// Called when pickup item is overlapped and collected or lost when AI spots the player
void APlayerPawn::UpdateCollection(int gold)
{
	goldAmount += gold;				// Adds the amound returned by the calling function (Pickup or AI sensing)
	if (goldAmount < 0)
	{
		goldAmount = 0;
	}
}

// Called each time a second is occurs in order to update match timer
void APlayerPawn::GameClock()
{
	clock = clock - 1;
	if (clock <= 0)
	{
		UGameplayStatics::SetGamePaused(GetWorld(), true);	// Sets Game to Pause when match timer expires
	}
}

// Called to update UI match timer
int APlayerPawn::GetCountdown()
{
	return clock;
}

// Called for sprinting
void APlayerPawn::Sprint() {	
	GetCharacterMovement()->MaxWalkSpeed *= SprintSpeedModifier;	// Adjust walkspeed to product of walk speed by sprint modifier
}

// Called when sprinting stopped
void APlayerPawn::StopSprinting() {
	GetCharacterMovement()->MaxWalkSpeed = 600;	// Adjust walk speed to quotiant of walk speed by sprint modifier
	GetWorldTimerManager().ClearTimer(SprintDelayHandle);			// Reset sprint delay timer
}	

// Called on jump
void APlayerPawn::OnStartJump() {
	bPressedJump = true;			// Sets player to jumping state 
}

// Called on jump end
void APlayerPawn::OnStopJump() {
	bPressedJump = false;			// Sets player to non jumping
}
