// Fill out your copyright notice in the Description page of Project Settings.

#include "AIControl.h"

void AAIControl::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), PoIs); // Finds number of Target Points in the level
	POIIndexs = PoIs.Num();																  // Assigns number of target points to varialbe

	GoNextPOI();																		  // Moves character to first target point
}

// Called when the AI character completes its target point movement
void AAIControl::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	if (bIsPathRandom)																				// If the AI us set to randomize is pathing
	{
		GetWorldTimerManager().SetTimer(Timer, this, &AAIControl::GoToRandomPOI, 3.0f, false);		// Reset teh time for the next ation to 3 second delay and than call the go to random point function
	}
	else
	{
		GetWorldTimerManager().SetTimer(Timer, this, &AAIControl::GoNextPOI, 3.0f, false);			// Reset teh time for the next ation to 3 second delay and than call the go to  next point function
	}
}

// Called to get the next target point in the level at random
ATargetPoint * AAIControl::GetRandomPOI()	
{
	auto index = FMath::RandRange(0, PoIs.Num() - 1);		// Randomly selects one of the target point in the level
	return Cast<ATargetPoint>(PoIs[index]);					// Returns the next randomized target point
}

// Called when AI is set to go to random target point
void AAIControl::GoToRandomPOI()
{
	MoveToActor(GetRandomPOI());					// Moves actor to location selected by random target point selection function
}

// Called when AI is set to move from one on target point to another in numeric order
void AAIControl::GoNextPOI()
{
	MoveToActor(PoIs[curPOIIndex]);					// Moves actor to next target point in order (by array index)
	curPOIIndex++;									// Iteration of target point index
	if (curPOIIndex >= POIIndexs - 1)				
	{
		curPOIIndex = 0;							// Set the current target point index to 0 if it was greater than equal to the number of target points
	}
}
