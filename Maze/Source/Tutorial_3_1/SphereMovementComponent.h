// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "SphereMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class TUTORIAL_3_1_API USphereMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()

public:

	// Declaration of function that updates movement
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	
};
