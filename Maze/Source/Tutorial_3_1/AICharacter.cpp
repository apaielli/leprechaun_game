// Fill out your copyright notice in the Description page of Project Settings.

#include "AICharacter.h"
#include "AIControl.h"
#include "PlayerPawn.h"



// Sets default values
AAICharacter::AAICharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensing"));	// Attach a pawn sensing component
	PawnSensingComp->SetPeripheralVisionAngle(30.0f);										// Sets the field of visual sensing


}

// Called when the game starts or when spawned
void AAICharacter::BeginPlay()
{
	Super::BeginPlay();
	if (PawnSensingComp)
	{
		PawnSensingComp->OnSeePawn.AddDynamic(this, &AAICharacter::OnPlayerSeen); // Enables pawn sensing component
	 }
}

// Called every frame
void AAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

// Called when AI senses player in vield of view
void AAICharacter::OnPlayerSeen(APawn * Pawn)
{
	class APlayerPawn* MyCharacter = Cast<APlayerPawn>(Pawn);		// Assign player pawn pointer in order to cast gold loss

	MyCharacter->UpdateCollection(-1);								// Removes one gold from the players total

}

