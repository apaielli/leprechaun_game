// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CameraControl.generated.h"

UCLASS()
class TUTORIAL_3_1_API ACameraControl : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraControl();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	APlayerController* PPController;		// Declare player controller pointer for PlayerPawn access

	UPROPERTY(EditAnywhere)					// Create input for selected actor
		AActor* CameraOne;					// as this pointer

	UPROPERTY(EditAnywhere)					// Create input for selected actor
		AActor* CameraTwo;					// as this pointer

	UPROPERTY(EditAnywhere)					// Create input for selected actor
		AActor* FollowCamera;				// as this pointer

	void SetCameraOne();					// Declaration of camera one set function
	void SetCameraTwo();					// Declaration of camera two set function
	void SetFollowCamera();					// Declaration of player camera set function

};
