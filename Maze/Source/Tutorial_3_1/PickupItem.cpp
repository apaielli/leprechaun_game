// Fill out your copyright notice in the Description page of Project Settings.

#include "PickupItem.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ShapeComponent.h"
#include "Engine.h"
#include "PlayerPawn.h"



// Sets default values
APickupItem::APickupItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PickupItemRoot = CreateDefaultSubobject<USceneComponent>(TEXT("PickupItemRoot"));								// Initialization of object root component
	RootComponent = PickupItemRoot;																					// Assignment of root component

	PickupItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupItemMesh"));							// Initialization of object mesh component
	PickupItemMesh->AttachToComponent(PickupItemRoot, FAttachmentTransformRules::SnapToTargetNotIncludingScale);	// Attachment of mesh to root component with fixed scaling

	PickupItemBox = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupItemBox"));									// Initialization of object overlap box
	PickupItemBox->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));														// Scaling of overlap box
	PickupItemBox->bGenerateOverlapEvents = true;																	// Set overlap bax to generate overlap even
	PickupItemBox->OnComponentBeginOverlap.AddDynamic(this, &APickupItem::OnPlayerEnterBox);						// Set overlap event to be triggered by player overlap only
	PickupItemBox->AttachToComponent(PickupItemRoot, FAttachmentTransformRules::SnapToTargetNotIncludingScale);		// Attachment of overlap box to root with fixed scaling

}

// Called when the game starts or when spawned
void APickupItem::BeginPlay()
{
}

void APickupItem::Tick(float DeltaTime)
{
}

// Called when object is overlapped by player
void APickupItem::OnPlayerEnterBox(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor != nullptr && OtherActor != this)
	{
		class APlayerPawn* MyCharacter = Cast<APlayerPawn>(OtherActor);						// Assign player pawn pointer in order to cast gold gain

		MyCharacter->UpdateCollection(1);													// Adds one gold to the players total
		Destroy();																			// Destroys the object
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Item Picked Up!"));	// Places debug message on screen to indicate functionality in overlap
	}
}

