// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "CameraControl.h"
#include "GameFramework/Character.h"
#include "PlayerPawn.generated.h"

UCLASS()
class TUTORIAL_3_1_API APlayerPawn : public ACharacter
{
	GENERATED_BODY()

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

public:
	// Sets default values for this character's properties
	APlayerPawn();

	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite)
	float SprintSpeedModifier;	// Sprint speed modifier variable


protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	const float BaseLookUpRate = 45.0f;			// Up and down look rate varialbe
	const float BaseTurnRate = 45.0f;			// Left and right look rate variable
	const float InitCameraZoom = 0.0f;			// Starting point for playr mounted camera (FP)
	const float MaxCameraZoom = 500.0f;			// Maximum zoom distanace
	const float MinCameraZoom = 0.0f;			// Minimum zoom distance (FP)
	const float CameraZoomTick = 25.0f;			// Rate of zoom per in and out function
	FRotator InitRotationRate = FRotator(0.0f, 540.0f, 0.0f);		// Base rotation rate
	FTimerHandle SprintDelayHandle;									//Timer handle variable
	FTimerHandle GameTimer;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	// Called for input setup
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	int goldAmount = 0;								// Variable for storing gold count
	int clock = 120;								// Variable for match timer

	UFUNCTION(BlueprintPure, Category = "Gold")		
		int GetGoldCount();							//Function for relaying gold count to UI

	void UpdateCollection(int gold);				//Function for updating gold count on item pickup

	void GameClock();								//Function for updating match timer

	UFUNCTION(BlueprintPure, Category = "Time")		//Function for relaying timer to UI
		int GetCountdown();



protected:

	float DefaultArmLength;		// Default Camera arm length variable
	float CameraZoom;			// Current zoom distance variable

	// Function called to update camera zoom distance
	void UpdateCameraArmLengths();

	// Called for Forward and Backward movements
	void ForwardBack(float Value);

	// Called for Left and Right Strafing
	void LeftRight(float Value);

	// Called for mouse control for left and right rotation
	void TurnAtRate(float TurnRate);

	// Called for mouse control for looking up and down
	void LookUpAtRate(float UpRate);


	// Function called for zooming camera in
	void CameraZoomIn();

	// Function called for zooming camera out
	void CameraZoomOut();

	UFUNCTION()
	void OnStartJump();	// Function for Jumping

	UFUNCTION()
	void OnStopJump();	// Function for ending Jumping

	// Called for sprinting
	void Sprint();

	// Called to end sprinting
	void StopSprinting();

	// Called when engaging sprint
	void SprintDelay();

	// Function for relaying camera input integer to set function
	template <int i>

	void ChangeView() { SetView(i); }

	void SetView(int camNum);
	
};
