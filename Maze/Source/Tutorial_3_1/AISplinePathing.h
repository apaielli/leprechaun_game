// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "Navigation/PathFollowingComponent.h"
#include "AISplinePathing.generated.h"

/**
 * 
 */
UCLASS()
class TUTORIAL_3_1_API AAISplinePathing : public AAIController
{
	GENERATED_BODY()
	
public:

	void BeginPlay() override;

	// Function for retrieving spline component ID and points
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;
	
private:

	USplineComponent* AISplinePath;		// Spline component pointer

	UFUNCTION()
		void GetNextPoints();			// Function for moving AI to next point in the spline

	int32 numbOfSplinePoints;			// Integer for storing number of point in spline
};
