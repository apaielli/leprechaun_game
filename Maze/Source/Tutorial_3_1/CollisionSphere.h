// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "CollisionSphere.generated.h"

UCLASS()
class TUTORIAL_3_1_API ACollisionSphere : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACollisionSphere();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	// Dclaration of the Sphere Movement component pointer
	class USphereMovementComponent* ObjMovementComponent;

	// Declaration of pawn movement function pointer
	virtual UPawnMovementComponent* GetMovementComponent() const override;

	// Forward and backward movement function declaration
	void ForwardBack(float value);

	// Side to side movement function declaration
	void Strafe(float value);

	// Mouse turn function declaration
	void Turn(float value);

	
	
};
