// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GAMPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TUTORIAL_3_1_API AGAMPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	virtual void BeginPlay() override;
	
	
};
