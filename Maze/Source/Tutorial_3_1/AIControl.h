// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"
#include "Engine.h"
#include "AIControl.generated.h"

/**
 * 
 */
UCLASS()
class TUTORIAL_3_1_API AAIControl : public AAIController
{
	GENERATED_BODY()
	
public:

	void BeginPlay() override;

	// Function for retrieving target point location by ID
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

	// Boolean variable for enabling and disabling random pathing
	UPROPERTY(EditAnywhere)
		bool bIsPathRandom = false;

private:
	
	// Array for storing all actor location of target point type
	UPROPERTY()
		TArray<AActor*> PoIs;

	// Timer variable for movement delays
	FTimerHandle Timer;

	//Pointer for retrieving random target point
	UFUNCTION()
		ATargetPoint* GetRandomPOI();

	// Function for moving the AI character to a random target point
	UFUNCTION()
		void GoToRandomPOI();

	// Function for moving the AI character to the next target point in the order
	UFUNCTION()
		void GoNextPOI();
	
	// Variable that store the number of target points
	int POIIndexs;

	// Variable that tracks the current target point location in iteration
	int curPOIIndex;

};
