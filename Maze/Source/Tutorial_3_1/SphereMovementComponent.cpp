// Fill out your copyright notice in the Description page of Project Settings.

#include "SphereMovementComponent.h"


void USphereMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	
	if (!PawnOwner || !UpdatedComponent || ShouldSkipUpdate(DeltaTime))		// If is not recieving movmeent input
	{
		return;																// Return the current values
	}

	FVector DesiredMovementThisFrame = ConsumeInputVector().GetClampedToMaxSize(1.0f) *DeltaTime * 150.0f;
	if (!DesiredMovementThisFrame.IsNearlyZero())							// If the object recieves movmeent input
	{
		FHitResult Hit;														// Check for collision
		SafeMoveUpdatedComponent(DesiredMovementThisFrame, UpdatedComponent->GetComponentRotation(), true, Hit);
		if (Hit.IsValidBlockingHit())										// If movement is not block by collition
		{
			SlideAlongSurface(DesiredMovementThisFrame, 1.0f - Hit.Time, Hit.Normal, Hit);  // Move object
		}
	}
	
}

