// Fill out your copyright notice in the Description page of Project Settings.

#include "AISplinePathing.h"

const int HEIGHT = 250;
const int MAXPOINTS = 5000;  
FVector pathPointLocation[MAXPOINTS];		// Vector for storing location of all spline points
int splinePoint = 1;						// integer storing the first index of the spline point vector
int totalSplinePoints = 0;					// integer storing total number of spline points

void AAISplinePathing::BeginPlay()
{
	Super::BeginPlay();

	GetNextPoints();									// Begins this function on play
	MoveToLocation(pathPointLocation[splinePoint]);		// Moves AI to first point in spline point sequence

}

// Called when the AI character completes movement from one spline point to the next
void AAISplinePathing::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	splinePoint++;							// Incriment spline point index
	if (splinePoint >= totalSplinePoints)	// If the spline index is the last in the vector
	{
		splinePoint = 1;					// Move back the the first index of the spline point vector location
	}
	MoveToLocation(pathPointLocation[splinePoint]);	// Move AI to that spline point
}

// Called to retrieve the next spline point
void AAISplinePathing::GetNextPoints()
{
	for (TObjectIterator<USplineComponent> SplineComponent; SplineComponent; ++SplineComponent)		// For each spline point location
	{
		int numOfSplinePoints = SplineComponent->GetNumberOfSplinePoints();		// retrieve the number of spline points
		float totalLength = SplineComponent->GetSplineLength();					// retrieve total length between spline points

		float currentLength = 0;												// Stores length between two spline points
		int itSpacing = 5;														// Default spline point spacing
		int smLength = 150;														// Default point to point length

		FString splName = SplineComponent->GetName();							// Retrieve the name of the spline component

		if (splName == "L1Spline")												// If the spline component name equals
		{
			int splPCount = 0;													// set spline point count to 0
			while (currentLength < totalLength)									// while current length is less than total length
			{
				FTransform splinePointTransform = SplineComponent->GetTransformAtDistanceAlongSpline(currentLength, ESplineCoordinateSpace::World);	// Move AI along spline
				currentLength += smLength;																											

				pathPointLocation[splPCount] = splinePointTransform.GetLocation();				// Moving Ai from spline point to next
				splPCount++;																	// Incriment spline point index
			}
			totalSplinePoints = splPCount;														// Set spline point index to last index or total number of spline points.
		}
	}
}


