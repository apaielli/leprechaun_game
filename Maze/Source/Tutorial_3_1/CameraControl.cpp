// Fill out your copyright notice in the Description page of Project Settings.

#include "CameraControl.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACameraControl::ACameraControl()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraControl::BeginPlay()
{
	Super::BeginPlay();
	PPController = UGameplayStatics::GetPlayerController(this, 0);		// Initialize player controller to variable
	FollowCamera = PPController->GetViewTarget();						// Initialize the Follow camera as player controller view
}

// Called every frame
void ACameraControl::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to set camera one actor as view
void ACameraControl::SetCameraOne()
{
	if (PPController) {
		if (PPController->GetViewTarget() != CameraOne) {		// If player controller view is not Camera one
			PPController->SetViewTarget(CameraOne);				// Set player controller view to camera one
		}
	}
}

// Called to set camera two actor as view
void ACameraControl::SetCameraTwo()
{
	if (PPController) {
		if (PPController->GetViewTarget() != CameraTwo) {		// If player controller view is not Camera two
			PPController->SetViewTarget(CameraTwo);				// Set player controller view to camera two
		}
	}
}

// Called to set the view to characters camera component
void ACameraControl::SetFollowCamera()
{
	if (PPController) {
		if (PPController->GetViewTarget() != FollowCamera) {	// If player controller view is not the player camera
			PPController->SetViewTarget(FollowCamera);			// Set player controller view to player camera
		}
	}
}

